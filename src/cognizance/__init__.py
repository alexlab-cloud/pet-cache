"""Flask app implementation."""

import os

from flask import Flask
from strawberry.flask.views import GraphQLView

from cognizance.settings import
# from api.schema import schema


def create_app(config_object=Config):
    """Create and configure the Flask application."""
    app = Flask(__name__, instance_relative_config=True)

    app.config.from_object(config=config)


