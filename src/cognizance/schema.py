"""Strawberry schema."""

from typing import Optional
from enum import Enum

import strawberry


@strawberry.enum
class Handedness(Enum):
    RIGHT = "right"
    LEFT = "left"
    AMBIDEXTROUS = "ambidextrous"
    MIXED = "mixed"


@strawberry.type
class Origin:
    title: str
    description: str


@strawberry.type
class Name:
    first_name: str
    middle_name: Optional[str] = strawberry.UNSET
    last_name: Optional[str] = strawberry.UNSET
    origin: Origin


@strawberry.type
class Backstory:
    title: str
    introduction: str
    body: str


@strawberry.type
class Character:
    name: Name
    age: int
    handedness: Handedness
    origin: Origin
    backstory: Backstory
