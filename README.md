# cognizance 🔮

![cognizance Wordmark](assets/images/logos/cognizance-wordmark-prototype.png "Cognizance Wordmark")

A service dedicated to staying mindful.

Access mindful ideas, habits, and goals that can help you feel in control of your life.

## Design

### Fonts

All logos, wordmarks, and headers should use [Simvoni Font](https://www.fontspace.com/simvoni-font-f54420).
