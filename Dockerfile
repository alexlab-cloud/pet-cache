FROM python:3.11-buster

ENV POETRY_HOME="/opt/poetry" \
    POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_IN_PROJECT=1 \
    POETRY_VIRTUALENVS_CREATE=1 \
    POETRY_CACHE_DIR=/tmp/poetry_cache

WORKDIR /app

RUN apt update && apt upgrade -y \
    # Install some system dependencies:
    && apt install --no-install-recommends -y \
        curl \
    # Install Poetry for package management:
    # https://github.com/python-poetry/poetry
    && curl -sSL 'https://install.python-poetry.org' | python -

ENV PATH="$PATH:$POETRY_HOME/bin"

# Initialize project w/ required dependencies only:
COPY pyproject.toml poetry.lock README.md ./
RUN poetry install --no-root --no-dev && rm -rf $POETRY_CACHE_DIR

# Copy over the source code and install anything else:
COPY /src/cognizance/ ./cognizance
RUN poetry install --without dev

# ENTRYPOINT ["poetry", "run", "python", "-m", "cognizance.main"]
